import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-stat-filters',
  templateUrl: './video-stat-filters.component.html',
  styleUrls: ['./video-stat-filters.component.css']
})
export class VideoStatFiltersComponent implements OnDestroy {
  filters: FormGroup;
  private destroy = new Subject();

  constructor(fb: FormBuilder, private vds: VideoDataService) {
    this.filters = fb.group({
      title: [''],
      author: ['']
    });

    this.filters.valueChanges.pipe(
      takeUntil(this.destroy),
    ).subscribe(value => this.vds.search(value));
  }

  ngOnDestroy() {
    this.destroy.next();
  }

}
