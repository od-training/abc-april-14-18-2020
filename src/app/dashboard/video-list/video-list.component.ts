import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';

import { VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css'],
})
export class VideoListComponent implements OnInit {
  videoList: Observable<Video[]>;
  highlightedVideo;
  @Output() onVideoSelected = new EventEmitter<Video>();

  constructor(vds: VideoDataService) {
    this.videoList = vds.videoList;
  }

  ngOnInit(): void {}

  setHighlightedVideo(videoData: Video) {
    this.onVideoSelected.emit(videoData);
    this.highlightedVideo = videoData;
  }
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetail[];
}

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}
