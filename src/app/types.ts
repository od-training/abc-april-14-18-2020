export interface FilterValues {
  title: string;
  author: string;
}
