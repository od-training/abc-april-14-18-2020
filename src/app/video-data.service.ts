import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Video } from './dashboard/video-list/video-list.component';
import { FilterValues } from './types';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  videoList: Observable<Video[]>;
  private _videoList: BehaviorSubject<Video[]> = new BehaviorSubject([]);
  private _filters: BehaviorSubject<FilterValues | undefined> = new BehaviorSubject(undefined);

  constructor(private _httpClient: HttpClient) {
    this.videoList = combineLatest([this._videoList, this._filters]).pipe(
      map(([videoList, filters]) => filterVideos(videoList, filters))
    );

    this.loadVideos().subscribe((list) => this._videoList.next(list));
  }

  search(filters: FilterValues) {
    this._filters.next(filters);
  }

  private loadVideos(): Observable<Video[]> {
    return this._httpClient.get<Video[]>(API);
  }
}

function filterVideos(videoList: Video[], filters: FilterValues | undefined): Video[] {
  return videoList.filter(video => {
    if (!filters) {
      return true;
    }
    return (!filters.title || video.title.toLowerCase().includes(filters.title.toLowerCase()))
      && (!filters.author || video.author.toLowerCase().includes(filters.author.toLowerCase()));
  });
}

const API = 'https://api.angularbootcamp.com/videos';
